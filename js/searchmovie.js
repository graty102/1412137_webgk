const images = 'https://image.tmdb.org/t/p/w500';
const movie = 'https://api.themoviedb.org/3/search/movie?api_key=89fe2cb4d55fb0631917dbc92b181408';
const person = 'https://api.themoviedb.org/3/search/person?api_key=89fe2cb4d55fb0631917dbc92b181408';

async function loadpage() {

    const reqstr = 'https://api.themoviedb.org/3/discover/movie?api_key=89fe2cb4d55fb0631917dbc92b181408&sort_by=popularity.desc';

    loading();

    const responese = await fetch(reqstr);
    const rs = await responese.json();
   
    listMovies(rs.results); 
}

function loading() {
    $('#main').empty();
    $('#main').append(`
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    `);
}

function listMovies(ms) {
    $('#main').empty();
    for( const m of ms) {
        const poster = images.concat(m.poster_path);
        $('#main').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100" >
                    <img class="card-img-top" src="${poster}" alt="${m.title}" onclick="InfoMovie('${m.id}')" >
                    <div class="card-body">
                        <h5 class="card-title">${m.title}</h5>
                        <h5 class="card-title">release_date: ${m.release_date}</h5>
                        <h5 class="card-title">vote_average: ${m.vote_average}</h5>
                        <p class="card-text">${m.overview}</p>
                        <button onclick="InfoMovie('${m.id}')"> chi tiet movie</button>

                    </div>
                </div>
            </div>
        `)
    }
}

async function InfoMovie(id) {
    const Info_movie = `https://api.themoviedb.org/3/movie/${id}?api_key=89fe2cb4d55fb0631917dbc92b181408`;

    loading();

    const responese = await fetch(Info_movie);
    const movie = await responese.json();
   
    Movie(movie);
}

function Movie(m) {
    const image = images.concat(m.poster_path);
   
    $('#main').empty();
    $('#main').append(`
        <div class="container">
            <h1 class="my-4">${m.title}</h1>
            <div class="row">
                <div class="col-md-7">
                    <img class="img-fluid" src="${image}" alt="${m.title}">
                </div>
                <div class="col-md-5">
                    <h3 class="my-3" >Plot</h3>
                    <p>${m.overview}</p>
                    <h6>Release_date: ${m.release_date}</h6>
                    <h3 class="my-3">Stars</h3>
                    <button onclick="showstar(${m.id})">Show</button> 
                    <h3 class="my-3">Review</h3>
                    <button onclick="showreview(${m.id})">Show</button> 
                </div>
            </div>
        </div>
    `);
}

async function showstar(id) {
    const star_movie = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=89fe2cb4d55fb0631917dbc92b181408`;

    loading();


    const responese_star = await fetch(star_movie);
    const rs = await responese_star.json();
   
    $('#main').empty();
    for (const m of rs.cast){
        const image = images.concat(m.profile_path);
        $('#main').append(`
            <div class="col-md-4 py-1">
                <div class="card shadow h-100">
                <div class="col-md-4">
                <img src="${image}" height="150px" onclick="InfoStar(${m.id})">
            </div>
            <div class="col-md-8">
                <p >Name: ${m.name}</p>
                <p >Character: ${m.character}</p>
            </div>
                </div>
            </div>
            `);
    }
}
async function search(e) {
    e.preventDefault();

    const strSearch = $('form input').val();
    const reqstr_movie = movie.concat("&query=", strSearch);
    const reqstr_star = person.concat("&query=", strSearch);

    loading();

    const responese_movie = await fetch(reqstr_movie);
    const rs_movie = await responese_movie.json();

    listMovies(rs_movie.results);

    const responese_star = await fetch(reqstr_star);    
    const rs_star = await responese_star.json();

    listMoviesStar(rs_star.results); 

}

async function listMoviesStar(ms) {
    for( const m of ms) {
        const poster = images.concat(m.profile_path);
        const id_star = `https://api.themoviedb.org/3/person/${m.id}/movie_credits?api_key=89fe2cb4d55fb0631917dbc92b181408`;
        
        const responese = await fetch(id_star);
        const rs = await responese.json();

        for( const n of rs.cast) {
            const poster = images.concat(n.poster_path);
            $('#main').append(`
                <div class="col-md-4 py-1">
                    <div class="card shadow h-100" onclick="InfoMovie('${n.id}')">
                        <img class="card-img-top" src="${poster}" alt="${n.title}">
                        <div class="card-body">
                            <h5 class="card-title">${n.title}</h5>
                            <h5 class="card-title">vote_average: ${n.vote_average}</h5>
                            <p class="card-text">${n.overview}</p>
                            <h5 class="card-title">release_date: ${n.release_date}</h5>
                        </div>
                    </div>
                </div>
            `)
        }
    }
}

async function InfoStar(id) {
    const api_info_star = `https://api.themoviedb.org/3/person/${id}?api_key=89fe2cb4d55fb0631917dbc92b181408`;
    
    loading();

    const responese = await fetch(api_info_star);
    const detail_star = await responese.json();
    
    const tamp = 'https://image.tmdb.org/t/p/h632';
    const poster = tamp.concat(detail_star.profile_path);

    const strSearch=detail_star.name;
    const reqstr_star = person.concat("&query=", strSearch);
    const responese_star = await fetch(reqstr_star);    
    const rs_star = await responese_star.json();

    listMoviesStar(rs_star.results); 
    $('#main').empty();
    $('#main').append(`
        <div class="container">
            <h1 class="my-4">${detail_star.name}</h1>
            <div class="row">
                <div class="col-md-7">
                    <img class="img-fluid" src="${poster}" alt="${detail_star.name}">
                </div>
                <div class="col-md-5">
                    <li>Birthday: ${detail_star.birthday}</li>
                    
                    <li>Place_of_birth: ${detail_star.place_of_birth}</li>
                    <li>Popularity: ${detail_star.popularity}</li>
                    <h3 class="my-3">Biography</h3>
                    <p>${detail_star.biography}</p>
                </div>
            </div>
        </div>
    `);
    
}

async function showreview(id) {
    const review_movie = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=89fe2cb4d55fb0631917dbc92b181408`;
    
    loading();

    const responese_view = await fetch(review_movie);
    const rs = await responese_view.json();
    
    $('#main').empty();
    for (const m of rs.results){
        $('#main').append(`
            <div>
                <h5 class="mt-0">${m.author}</h5>
                <p>${m.content}</p>
            </div>
            `);
    }
}